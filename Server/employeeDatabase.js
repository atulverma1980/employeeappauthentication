var mysql = require('mysql');
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var passport = require("passport");
var passportjwt = require("passport-jwt");
var config = require("./src/node/N3/config.js");
var ExtractJwt = passportjwt.ExtractJwt;
var Strategy=passportjwt.Strategy;
var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers,Origin,Accept,Authorization,X-Requested-With,Content-Type,Access-Control-Request");
  res.header("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
  res.header("Access-Control-Allow-Credentials",true);
  res.header("Access-Control-Expose-Headers","*");
  next();
  });
  var params={
    secretOrKey:config.jwtSecret,
    jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken()
  };
  app.use(bodyParser.json());
  passport.initialize();
  app.listen(2410,()=>console.log("Listening to the Port 2410"));
  const jwtExpirySeconds=300
//creating connection 
var con = mysql.createConnection({
  host :"localhost",
  user: "root",
  password :"root",
  database:"mydb",
});
//connecting to the database
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query("create database if not exists mydb",function(err,result){
      if(err) throw err;
      console.log("Database Created");
});
  var sql = "create table if not exists employees (id varchar(15) ,name varchar(20),department varchar(20),designation varchar(20),salary integer(20),gender varchar(15),email varchar(30),password varchar(20),primary key(id))";
  con.query(sql,function(err,result){
      if(!err){
        insertData();
        console.log("table created");
       } else{
        throw err;
       }
    });
 
});
//Inserting values to the created table
 insertData=()=>{
      var sql = "insert ignore into employees values?";
      var values=[
        ["AB1234","Jack","Finance","Trainee","15000","Male","jack@gmail.com","jack123"],
        ["AB4321","Mary","Operation","Junior Manager","18000","Female","mary@gmail.com","mary123"],
        ["BA1234","Edward","Technology","Manager","25000","Male","edward@gmail.com","edward123"],
        ["BA4321","Juli","HR","General Manager","35000","Female","juli@gmail.com","juli123"],
        ["QW3456","Sumit","Finance","Vice Present","40000","Male","sumit@gmail.com","sumit123"],
        ["QA1245","Mohit","Operation","Manager","20000","Male","mohit@gmail.com","mohit123"],
        ["CF4689","Rohan","Technology","Trainee","15000","Male","rohan@gmail.com","rohan123"],
        ["MK8811","Govind","HR","Vice Present","40000","Male","govind@gmail.com","govind123"],
        ["LK6590","Neha","Finance","Junior Manager","20000","Female","neha@gmail.com","neha123"],
        ["UY6578","Mark","","","","Male","mark@gmail.com","mark123"],
        ["XF7821","Jhon Dav","","","","Female","jhon@gmail.com","jhon123"],
        ["GH2134","Vikram","Finance","Manager","20000","Male","vikram@gmail.com","vikram123"],
        ["MK4221","Nimit","Technology","Trainee","14000","Male","nimit@gmail.com","nimit123"],
        ["XC1122","Aparna","Finance","Manager","30000","Female","aprna@gmail.com","aparna123"],
        ["WE2332","Shyam","Operation","Vice Present","50000","Male","shyam@gmail.com","shyam123"],
        ["RX2378","Daniel","HR","Junior Manager","25000","Male","daniel@gmail.com","daniel123"],
        ["FG4333","Cristi","Technology","Trainee","15000","Female","cristi@gmail.com","cristi123"],
        ["DS3344","Romeo","Finance","Trainee","20000","Male","romeo@gmail.com","romeo123"],
        ["MH4222","Monika","HR","Junior Manager","20000","Female","monika@gmail.com","monika123"],
        ["UU7456","Elon","Operation","Manager","25000","Male","elon@gmail.com","elon123"],
        ["DR8356","Cris Moris","Technology","Trainee","12000","Male","cris@gmail.com","cris123"],
        ["VB6578","Rekha","Finance","Trainee","13500","Female","rekha@gmail.com","rekha123"],
        ["HG4678","Rohit","","","","Male","rohit@gmail.com","rohit123"],
        ["FG8746","Sohan","","","40000","Male","sohan@gmail.com","sohan123"],
        ["TG6743","Gaurav","Finance","","13000","Male","gaurav@gmail.com","gaurav123"],
        ["HY7781","Sanjeev","Operation","Trainee","20000","Male","sanjeev@gmail.com","sanjeev123"],
        ["OK7467","Anubhav","","","","Male","anubhav@gmail.com","anubhav123"],
        ["GH3466","Lokesh","Finance","Manager","20000","Male","lokesh@gmail.com","lokesh123"],
        ["RT6356","Ritika","HR","Trainee","10000","Female","ritika@gmail.com","ritik123"],
        ["XH1526","Rachit","Technology","Vice Present","40000","Male","rachit@gmail.com","rachit123"],
      ]
      con.query(sql,[values],function(err,result){
          if(err) throw err;
          console.log("rows affected "+result.affectedRows);
      });
      // con.query("select * from employees",(err,result)=>{
      //   if(err) throw err;
      //   // let x=result.findIndex(v=>v.id==="UY6578")
      //   console.log(result[3]);
      // });
 }
 var strategy = new Strategy(params,function(payload,done){
  // console.log("I m calling");
  let sqlQuery = 'select * from employees';
  con.query(sqlQuery,(err,result,field)=>{
    if(err) throw err;
    let index = result.findIndex(v=>v.id===payload.id);
    var user=result[index]||null;
    if(user){
      return done(null,{
          id:user.id
      })
  }else{
      return done(new Error("User not Found"),null);
  }
  });
});
passport.use(strategy);

app.post("/login",(req,res)=>{
  if(req.body.email && req.body.password){
    var email=req.body.email;
    var password=req.body.password;
    con.query("select * from employees",(err,result,field)=>{
      if(err) throw err;
      var user = result.find(v=>v.email===email && v.password===password);
      if(user){
        var payload={
          id:user.id
        };
        var token=jwt.sign(payload,config.jwtSecret,{
          algorithm:"HS256",
          expiresIn:jwtExpirySeconds
        });
      
        res.setHeader("X-Auth-Token",token);
        res.json({success:true,token:"bearer "+token})
      }else{
        res.sendStatus(401);
      }
    })
  }else{
    res.sendStatus(401);
  }
})
//get employee
 app.get("/employee",passport.authenticate("jwt",{session:false}),(req,res)=>{
  const params=[];
  let sqlQuery = 'select * from employees where 1 = 1';
  let page = req.query.page ? +req.query.page : 1;
  let department= req.query.department?req.query.department.split(","):undefined;
  let gender= req.query.gender;
  let sortBy=req.query.sortBy;
  let salary = req.query.salary;
  let designation = req.query.designation?req.query.designation.split(","):undefined;
  if(department!==undefined){
    sqlQuery+=` and department in (${department.map(v=>`"${v}"`)})`;
  }
  if(designation!==undefined){
    sqlQuery+=` and designation in (${designation.map(v=>`"${v}"`)})`;
  }
  if(gender!==undefined){
    sqlQuery+=' and gender=?';
    params.push(gender);
  }
  if(salary!==undefined){
    if(salary=="<15000"){
        sqlQuery+=' and salary<15000';
    }
    if(salary=="15-25000"){
        sqlQuery+=' and salary between 15000 and 25000';
    }
    if(salary==">25000"){
        sqlQuery+=' and salary>25000';
    }
  }
    con.query(sqlQuery,params,(err,result,fields)=>{
      if(err) throw err;
  if(sortBy!==undefined){
  let arr;
  switch(sortBy){
    case "salary":arr = result.sort((s1,s2)=>(s1.salary-s2.salary));break;
    case "designation":arr=result.sort((s1,s2)=>s1.designation.localeCompare(s2.designation));break;
    case "department":arr=result.sort((s1,s2)=>s1.department.localeCompare(s2.department));break;
    case "id":arr=result.sort((s1,s2)=>s1.id.localeCompare(s2.id));break;
    case "name":arr=result.sort((s1,s2)=>s1.name.localeCompare(s2.name));break;
    case "gender":arr=result.sort((s1,s2)=>s1.gender.localeCompare(s2.gender));break;
    default:break;
    }
      result=arr;
  }
       res.send(makeData(page,6,result));
    });
});
//make data paginated
let makeData = (pageNum, size, data1) => {
    let startIndex = (pageNum - 1) * size;
    let endIndex =
      data1.length > startIndex + size - 1
        ? startIndex + size - 1
        : data1.length - 1;
    let data2 = data1.filter(
      (lt, index) => index >= startIndex && index <= endIndex
    );
    let dataFull = {
      startIndex: data1.length > 0 ? startIndex + 1 : startIndex,
      endIndex: data1.length > 0 ? endIndex + 1 : endIndex,
      numOfItems: data1.length,
      data: data2,
    };
    return dataFull;
  };
  //post employee
  app.post("/employee/add",(req,res)=>{
    let body = req.body;
    let sqlQuery = 'insert into employees values?';
    let values=[
      [body.id,body.name,body.department,body.designation,body.salary,body.gender],
    ];
    con.query(sqlQuery,[values],(err,result)=>{
      if(err){
        res.status(401).send('Id must be unique');
        throw err;
      }
      console.log("row affected :"+result.affectedRows);
      res.send(body);
    })
  });
  //update employee
  app.put("/employee/edit/:id",(req,res)=>{
    let id = req.params.id;
    let body = req.body;
    let sqlQuery = `update employees set name="${body.name}", department="${body.department}", designation="${body.designation}", salary=${body.salary}, gender="${body.gender}" where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err) throw err
      console.log("row Updated :"+result.affectedRows+" ID :-",id);
      res.send(body);
    })
  });
  //delete employee
  app.delete("/employee/delete/:id",(req,res)=>{
    let id = req.params.id;
    let sqlQuery = `delete from employees where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err)throw err
      console.log("Record Deleted having id :-"+id);
    })
  });
  //get particular seleted employee
  app.get("/employee/:id",(req,res)=>{
    let id = req.params.id;
    let sqlQuery = `select * from employees where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err) throw err;
      res.send(result);
    })
  });
  //Resetting table
  app.delete("/employee/reset",(req,res)=>{
    let sqlQuery = 'delete from employees';
    con.query(sqlQuery,(err,result)=>{
      if(err)throw err;
      console.log("Table is Resetting");
      insertData();
    });
  
  });
