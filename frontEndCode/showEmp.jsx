import React from "react";
import http from "../../services/httpService";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import {Link} from "react-router-dom";
import queryString from "query-string";
import LeftPanel from "./leftPanelEmp";
import axios from "axios";
export default class ShowEmp extends React.Component{
    state={
        detail:[],
       
    }
    async fetchData(){
        const queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchStr(queryParams);
        // let response = await http.get(`/employee?${searchStr}`);
        // let{data}=response;
        // console.log(data);
        // this.setState({detail:data});
        const user = localStorage.getItem("user");
        try {
            var resp= await axios.get(`http://localhost:2410/employee?${searchStr}`,{
                headers:{Authorization:user}
            });
     
            let{data}=resp;
          
            this.setState({detail:data});
        } catch (er){
            if(er.response && er.response.status==401){
                localStorage.removeItem("user");
                window.location="/login";
            }
        }
    }
     componentDidMount(){
      this.fetchData()
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
            this.fetchData();
        }
    }
    handleChange=(opt)=>{
        opt.page=1;
        this.callURL("/employee",opt);
    }
    callURL=(url,option)=>{
        let searchStr = this.makeSearchStr(option);
        this.props.history.push({
            pathname:url,
            search:searchStr
        });
    }
    makeSearchStr=(option)=>{
        const{department,designation,salary,gender,sortBy,page}=option;
        let searchStr="";
        searchStr=this.addToQuery(searchStr,"department",department);
        searchStr=this.addToQuery(searchStr,"designation",designation);
        searchStr=this.addToQuery(searchStr,"gender",gender);
        searchStr=this.addToQuery(searchStr,"sortBy",sortBy);
        searchStr=this.addToQuery(searchStr,"salary",salary);
        searchStr=this.addToQuery(searchStr,"page",page);
        return searchStr;
    }
    sort=(colNo)=>{
        const queryParams = queryString.parse(this.props.location.search);
        switch (colNo) {
        case 0:
             queryParams.sortBy="id";
             this.callURL("/employee",queryParams);
             break;
        case 1:  
             queryParams.sortBy="name";
             this.callURL("/employee",queryParams);
             break;
        case 5:
             queryParams.sortBy="salary";
             this.callURL("/employee",queryParams);
             break;
        case 3:
            queryParams.sortBy="department";
            this.callURL("/employee",queryParams); 
            break;
        case 4:
            queryParams.sortBy="designation";
            this.callURL("/employee",queryParams);   
            break;
        case 2:
            queryParams.sortBy="gender";
            this.callURL("/employee",queryParams);
             break;
         default:break;         
        }
    }
    handlePage=(incr)=>{
        const queryParam = queryString.parse(this.props.location.search);
        let {page="1"}=queryParam;
        let newPage = +page+incr;
        queryParam.page=newPage;
        this.callURL("/employee",queryParam);
    }
    addToQuery=(str,name,val)=>
    val?str?`${str}&${name}=${val}`:`${name}=${val}`:str;
    render(){
        const{data=[],startIndex,endIndex,numOfItems}=this.state.detail;
        const queryParams = queryString.parse(this.props.location.search);
        return <div className="container my-4 bg-light">
            <div className="row">
                <div className="col-2">
                    <LeftPanel  option={queryParams} onOptionChange={this.handleChange}/>
                </div>
                <div className="col-10">
                    Showing {startIndex} to {endIndex} of {numOfItems}
            <div className="row bg-info text-dark text-center">
                <div className="col-1 border"onClick={()=>this.sort(0)}>Id</div>
                <div className="col-2 border"onClick={()=>this.sort(1)}>Name</div>
                <div className="col-2 border"onClick={()=>this.sort(2)}>Gender</div>
                <div className="col-2 border"onClick={()=>this.sort(3)}>Department</div>
                <div className="col-2 border"onClick={()=>this.sort(4)}>Designation</div>
                <div className="col-2 border"onClick={()=>this.sort(5)}>Salary</div>
                <div className="col-1 border"></div>
            </div>
            {data.map(v=><div className="row text-center" key={v.id}>
            <div className="col-1 border">{v.id}</div>
                <div className="col-2 border">{v.name}</div>
                <div className="col-2 border">{v.gender}</div>
                <div className="col-2 border">{v.department}</div>
                <div className="col-2 border">{v.designation}</div>
                <div className="col-2 border">{v.salary}</div>
                <div className="col-1 border">
               <Link to={`/editEmp/${v.id}`}><FontAwesomeIcon className="m-1" icon={faEdit}/></Link>
               <Link to={`/Employee/${v.id}`}> <FontAwesomeIcon className="m-1" icon={faTrash}/></Link>
                </div>
            </div>)}
            <div className="row">
                <div className="col-2">
                {startIndex>1?<button className="btn btn-success" onClick={()=>this.handlePage(-1)}>Previous</button>:""}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                {endIndex==numOfItems?"":<button className="btn btn-success"onClick={()=>this.handlePage(1)}>Next</button>}
                </div>
            </div>

            </div>
            </div>
        </div>
    }
}