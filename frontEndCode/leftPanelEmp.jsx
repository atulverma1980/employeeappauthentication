import React from "react";
export default class LeftPanel extends React.Component{
    state={
       dept:["Finance", "Operation", "Technology", "HR"],
       desg:["Trainee", "Junior Manager", "Manager", "General Manager", "Vice President"],
       gend:["Male","Female"],
       salry:["<15000", "15-25000", ">25000"],
    }
    makeRadio=(arr,value,name,label)=>(
        <React.Fragment>
         <label className="font-weight-bold bg-light">{label}</label>
         {arr.map(v=><div className="form-check"key={v}>
                <input type="radio" className="form-check-input" name={name} value={v}
                checked={value==v} onChange={this.handleChange}/>
                <label className="form-check-label">{v}</label>
            </div>)}
        </React.Fragment>
    )
    makeCB=(arr,value,name,label)=>(
        <React.Fragment>
            <label className="font-weight-bold">{label}</label>
            {arr.map(v=><div className="form-check"key={v}>
                <input type="checkbox" className="form-check-input" name={name} value={v}
                checked={value.findIndex(a=>a===v)>=0} onChange={this.handleChange}/>
                <label className="form-check-label">{v}</label>
            </div>)}
        </React.Fragment>
    );
    // handleChange=(e)=>{
    //     const{currentTarget:input}=e;
    //     let opt = {...this.props.option};
    //     opt[input.name]=this.updateCB(opt[input.name],input.checked,input.value);
    //     this.props.onOptionChange(opt);
    // }
    updateCB=(inpVal,checked,value)=>{
        let inpArr = inpVal?inpVal.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex(v=>v===value);
            if(index>=0){
                inpArr.splice(index,1);
            }
        }
        return inpArr.join(",");
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1={...this.props.option};
        input.type==="checkbox"?s1[input.name]=this.updateCB(s1[input.name],input.checked,input.value):s1[input.name]=input.value;
        this.props.onOptionChange(s1);
    }
    render(){
        const{dept,desg,salry,gend}=this.state;
        const{department="",designation="",salary,gender}=this.props.option;
        return<div className="row">
            <div className="col-12">
            {this.makeCB(dept,department.split(","),"department","Choose Department")}
            {this.makeCB(desg,designation.split(","),"designation","Choose Designation")}
            {this.makeRadio(gend,gender,"gender","Choose Gender")}
            {this.makeRadio(salry,salary,"salary","Choose salary")}

            </div>
        </div>
    }
}