import axios from "axios";
import React from "react";
import http from "../../services/httpService";
export default class Delete extends React.Component{

    async componentDidMount(){
        let {id}=this.props.match.params;
        let response  = await axios.get(`http://localhost:2410/employee/${id}`);
        let {data}=response;
        this.Delete(`http://localhost:2410/employee/delete/${id}`,data);
        this.props.history.push("/Employee");
    }
    async Delete(url,obj){
        let response= await axios.delete(url,obj);
    }

    render(){
      
        return <div></div>
    }
}