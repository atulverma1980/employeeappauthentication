import React,{Component} from "react";
import {Link} from "react-router-dom";
class Navbar extends Component{
    render(){
       const user = localStorage.getItem("user");
        return(
            <nav className="navbar navbar-expand-sm navbar-success  bg-success">
              
                <Link to="/" className="navbar-brand text-dark">
                    Employees
                </Link>
                <div className="">
                    <ul className="navbar-nav mr-auto">
                        {user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/Employee">
                                Show Employee
                            </Link>
                        </li>
                        )}
                        {user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/addemployee">
                                Add Employee
                            </Link>
                        </li>)}
                        {user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/reset">
                                Reset 
                            </Link>
                        </li>)}
                        {!user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/login">
                                Login 
                            </Link>
                        </li>)}
                        {user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/logout">
                                Logout 
                            </Link>
                        </li>)}
                    </ul>
                </div>
            </nav>
        )
    }
} 
export default Navbar;