import axios from "axios";
import React from "react";
import http from "./../../services/httpService";
export default class ResetAll extends React.Component{
   async componentDidMount(){
    window.location="/Employee";
       let response = await axios.delete("http://localhost:2410/employee/reset");
   }
   
   render(){
       return <h5>Resetting table...</h5>
   }
}