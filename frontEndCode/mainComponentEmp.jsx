import React from "react";
import {Route,Switch,Redirect} from "react-router-dom";
import AddEmployee from "./addEmployee";
import Delete from "./deleteEmp";
import Login from "./empLogin";
import Logout from "./empLogout";
import Navbar from "./NavbarEmp";
import ResetAll from "./resetEmp";
import ShowEmp from "./showEmp";
export default class MainComponent extends React.Component{

    render(){
        const user = localStorage.getItem("user");
        return <div className="container-fluid">
            <Navbar/>
            <Switch>
            <Route path="/login" component={Login}/>
                <Route path="/logout" component={Logout}/>
                <Route path="/Employee/:id" 
                render={(props)=>user?<Delete {...props}/>:<Redirect to="/login"/>}/>
                 <Route path="/editEmp/:id" 
                 render={(props)=>user?<AddEmployee {...props}/>:<Redirect to="/login"/>}/>
                 <Route path="/addemployee"
                 render={(props)=>user?<AddEmployee {...props}/>:<Redirect to="/login"/>}/>
                <Route path="/Employee" 
                render={(props)=>user?<ShowEmp {...props}/>:<Redirect to="/login"/>}/>
                <Route path="/reset"
                render={(props)=>user?<ResetAll {...props}/>:<Redirect to="/login"/>}/> 
                <Redirect from="" to ="/login"/>
            </Switch>
        </div>
    }
}