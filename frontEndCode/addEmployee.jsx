import axios from "axios";
import React from "react";
import http from "../../services/httpService";
export default class AddEmployee extends React.Component{
    state={
        detail:{id:"",name:"",gender:"",department:"",designation:"",salary:""},
        dept:["Finance", "Operations", "Technology", "HR"],
        desig:["Trainee", "Junior Manager", "Manager", "General Manager", "Vice President"],
        edit:false,
    }
    async componentDidMount(){
        let {id}=this.props.match.params;
        if(id){
        let response = await axios.get(`http://localhost:2410/employee/${id}`);
        let {data}=response;
        let s1={...this.state};
        data.map(v=>{
            s1.detail.id=v.id;
            s1.detail.name=v.name;
            s1.detail.gender=v.gender;
            s1.detail.department=v.department;
            s1.detail.designation=v.designation;
            s1.detail.salary=v.salary;
        });
        s1.edit=true;
        this.setState(s1);
        }
    }
    handleChange=e=>{
     const{currentTarget:input}=e
     let s1={...this.state};
     s1.detail[input.name]=input.value;
     this.setState(s1);
    }
    async postdata(url,obj){
        let response = await axios.post(url,obj);
        this.props.history.push("/Employee");
    }
    async putdata(url,obj){
        let response = await axios.put(url,obj);
        this.props.history.push("/Employee");
    }
    handleSubmit=e=>{
        e.preventDefault();
        let s1={...this.state};
        s1.edit?this.putdata(`http://localhost:2410/employee/edit/${s1.detail.id}`,s1.detail):
        this.postdata("http://localhost:2410/employee/add",s1.detail);
    }

    render(){
        const{id,name,gender,department,designation,salary}=this.state.detail;
        const{dept,desig}=this.state;
       
        return <div className="container">
            <div className="form-group">
                <label>Id</label>
                <input type="text" name="id" readOnly={this.state.edit} value={id} className="form-control" onChange={this.handleChange}/>
            </div>
            <div className="form-group">
                <label>Name</label>
                <input type="text" name="name" value={name} className="form-control" onChange={this.handleChange}/>
            </div>
           
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Gender:-</label>
                <div className="col-10">
                    <div className="row">
                        <div className="col-4">
                            <label>Male</label>
                            <input type="radio" name="gender" value="Male" checked={gender=="Male"} onChange={this.handleChange}/>
                        </div>
                        <div className="col-4">
                            <label>Female</label>
                        <input type="radio" name="gender" value="Female" checked={gender=="Female"} onChange={this.handleChange}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Dept:-</label>
                <div className="col-10">
                    <select className="form-control"  name="department" value={department} onChange={this.handleChange}>
                        <option value="">Select Department</option>
                        {dept.map(v=><option key={v}>{v}</option>)}
                    </select>
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Designation:-</label>
                <div className="col-10">
                    <select className="form-control"  name="designation" value={designation} onChange={this.handleChange}>
                        <option value="">Select designation</option>
                        {desig.map(v=><option key={v}>{v}</option>)}
                    </select>
                </div>
            </div>
            <div className="form-group">
                <label>Salary</label>
                <input type="text" name="salary" value={salary} className="form-control" onChange={this.handleChange}/>
            </div>
          
            <div className="text-center">
                <button className="btn btn-primary" onClick={this.handleSubmit}>{this.state.edit?"Update":"Submit"}</button>
            </div>
        </div>
    }
}